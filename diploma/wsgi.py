# -*- coding: utf-8 -*-

import os, site

BASEDIR = os.path.dirname(os.path.realpath(__file__))

# put local packages in path
site.addsitedir(BASEDIR)
site.addsitedir(os.path.join(BASEDIR, '../env/lib/python2.7/site-packages'))

# virtualenv activation
activate_this = os.path.join(BASEDIR, '../env/bin/activate_this.py')
execfile(activate_this, dict(__file__ = activate_this))

# get project settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'diploma.settings')

# run project as an WSGI application
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
