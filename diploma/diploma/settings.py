# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'), )

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'usizy5pda5!&r7)$p+6@my2eakzkvpa6al&gc4o-pupjxfg0nq'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['diploma.codetinkers.ru', '127.0.0.1', ]

SITE_ID = 1

APPEND_SLASH = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = '127.0.0.1'
EMAIL_PORT = 25
#EMAIL_HOST_USER = 'diploma@codetinkers.ru'
#EMAIL_HOST_PASSWORD = 'ZUcfWt5QvaLV'
SERVER_EMAIL = 'diploma@codetinkers.ru'

# Application definition

INSTALLED_APPS = (
    'autocomplete_light',
    'suit',
    'suit_ckeditor',
    # built-in
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    # 3rd party
    'dbsettings',
    'bootstrap_toolkit',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    #'allauth.socialaccount.providers.vk',
    'allauth.socialaccount.providers.twitter',
    'allauth.socialaccount.providers.tumblr',
    'avatar',
    # custom
    'profile',
    'scrapped',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    # allauth processors
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
    # custom
    'diploma.context_processors.site_processor',
)

AUTHENTICATION_BACKENDS = (
    # default authentication (username/password)
    'django.contrib.auth.backends.ModelBackend',
    # allauth backend
    'allauth.account.auth_backends.AuthenticationBackend',
)

# this is to integrate our profile extension
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'first_name', ]

AVATAR_STORAGE_DIR = 'avatar/'

ROOT_URLCONF = 'diploma.urls'

WSGI_APPLICATION = 'wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'diploma',
        'USER': 'diploma',
        'PASSWORD': '9K3VD259634b',
        'HOST': '127.0.0.1',
        'PORT': 3306,
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static/'), )
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '../static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../media/')

FILE_UPLOAD_PERMISSIONS = 0o644

SUIT_CONFIG = {
    'MENU': (
        'sites',
        'auth',
        {'app': 'socialaccount', 'label': 'Соцсети', 'icon': 'icon-user'},
        {'app': 'flatpages', 'label': 'Простые страницы', 'icon': 'icon-file'},
        {'app': 'scrapped', 'label': 'Новости'},
        {'url': '/settings/', 'label': 'Настройки', 'icon': 'icon-cog'},
        )
    }

# Используем файлы Bootstrap из поставки Suit

BOOTSTRAP_BASE_URL = os.path.join(STATIC_URL, 'suit/bootstrap/')

ACCOUNT_LOGOUT_ON_GET = True
