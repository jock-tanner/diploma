# coding: utf-8

from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^settings/', include('dbsettings.urls')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^tags/', 'profile.views.user_profile'),
    # custom
    url(r'^$', 'diploma.views.home'),
    # catchall
    url(r'^(?P<url>.*/)$', include('django.contrib.flatpages.urls')),
)
