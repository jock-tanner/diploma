# coding: utf-8

from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from allauth.socialaccount.models import SocialAccount

from diploma.decorators import render_to
from profile.models import UserProfile
from scrapped.models import News


PAGE_COUNT = 10


@login_required()
@render_to('home.html')
def home(request):
    """
    Домашняя страница (временная) (список тэгов и статей)
    """
    context = {}
    context['tags'] = UserProfile.objects.get(user=request.user).tag.all()
    paginator = Paginator(News.objects.filter(tag__in=context['tags']), PAGE_COUNT)
    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)
    context['vk'] = 'vk' in SocialAccount.objects.filter(user=request.user).values_list('provider', flat=True)
    context['news'] = news
    return context
