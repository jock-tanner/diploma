# coding: utf-8

from __future__ import unicode_literals

from django.shortcuts import redirect
from django.contrib import messages

from diploma.decorators import render_to
from profile.forms import UserProfileForm
from profile.models import UserProfile


@render_to('tags.html')
def user_profile(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = UserProfileForm(instance=UserProfile.objects.get(user=request.user),
                                   data=request.POST, files=request.FILES)
            if form.is_valid():
                form.save()
                messages.success(request, 'Тэги сохранёны.')
            else:
                messages.error(request, 'Ошибка сохранения тэгов!')
        context = {}
        context['form'] = UserProfileForm(instance=UserProfile.objects.get(user=request.user))
        return context
    else:
        return redirect('/')
