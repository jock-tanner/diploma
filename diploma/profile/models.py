# coding: utf-8

from __future__ import unicode_literals, print_function

import requests, tempfile, os, pytumblr
from django.core.files import File
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.dispatch import receiver
from allauth.account.signals import user_logged_in
from allauth.socialaccount.models import SocialApp, SocialAccount, SocialToken
from avatar.models import Avatar

from scrapped.models import Tag


# create a profile for a new user
# создаём профиль для нового пользователя
@receiver(post_save, sender=User)
def on_user_create(sender, instance, created, **kwargs):
    if created:
        profile = UserProfile(user=instance)
        profile.save()


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    tag = models.ManyToManyField(Tag, verbose_name='Тэги', null=True, blank=True)

    def __init__(self, *args, **kwargs):
        # storage for posted property
        self._posted = list()
        super(UserProfile, self).__init__(*args, **kwargs)

    @property
    def posted(self):
        """
        Список постов, которые уже были опубликованы для этого пользователя
        за текущее обращение к постеру. Это могло бы быть поле m2m, но у нас
        нет причины хранить этот список в базе данных.
        """
        return self._posted

    @posted.setter
    def posted(self, value):
        self._posted = value

    @posted.deleter
    def posted(self):
        del self._posted

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Расширенный профиль'


def save_avatar(pic_url, user):
    """
    Процедура сохранения аватара в модели django-avatar
    procedure for saving avatar to django-avatar model

    :param pic_url: строка − URL аватара
    :param sociallogin: объект входа через социальный сервис
    """
    pic = requests.get(pic_url)
    pic_tmp = tempfile.NamedTemporaryFile()
    for chunk in pic.iter_content(4096):
        pic_tmp.write(chunk)
    avatar, created = Avatar.objects.get_or_create(user=user, primary=True)
    avatar.avatar.save(os.path.basename(pic.url), File(pic_tmp))
    pic_tmp.close()
    avatar.save()


@receiver(user_logged_in)
def on_user_logged_in(request, user, **kwargs):
    """
    В процессе входа через соцсеть извлекаем аватар этой соцсети и сохраняем его
    в соответствующей модели компонента django-avatar
    """
    account = SocialAccount.objects.filter(user=user)
    if account:
        account = account[0]
    else:
        return
    if account.provider == 'twitter':
        pic_url = account.extra_data.get('profile_image_url')
        save_avatar(pic_url, user)
    elif account.provider == 'tumblr':
        # готовим начальные параметры
        # preparing initial parameters
        social_app = SocialApp.objects.get(provider='tumblr')
        consumer_key = social_app.client_id
        consumer_secret = social_app.secret
        social_token = SocialToken.objects.get(app=social_app, account=account)
        access_key = social_token.token
        access_secret = social_token.token_secret
        # создаём клиента
        # create client
        client = pytumblr.TumblrRestClient(
            consumer_key,
            consumer_secret,
            access_key,
            access_secret,
        )
        # get user name
        # получаем имя пользователя
        user_name = client.info()['user']['name']
        # получаем аватар
        # get avatar
        pic_url = client.avatar(user_name)['avatar_url']
        save_avatar(pic_url, user)
