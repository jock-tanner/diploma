# coding: utf-8

from __future__ import unicode_literals

from autocomplete_light import ModelForm

from profile.models import UserProfile


class UserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ['tag', ]
        labels = {
            'tag': 'Введите интересующие вас тэги (от 1 до 12)',
        }
