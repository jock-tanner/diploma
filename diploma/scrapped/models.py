# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
import autocomplete_light

from values import (BigmirOptions, IxbtOptions, RiaOptions, RbkOptions, ItworldOptions,
                    ComnewsOptions, RosbaltOptions, InterfaxOptions, BeltaOptions, ForbesOptions)

# Create your models here.

class Tag(models.Model):
    name = models.CharField(verbose_name='Тэг', max_length=128, unique=True)

    class Meta:
        ordering = ['name', ]
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'

    def __unicode__(self):
        return self.name


class News(models.Model):
    date = models.DateTimeField(verbose_name='Дата и время публикации')
    link = models.CharField(verbose_name='Оригинал', max_length=255)
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    text = models.TextField(verbose_name='Текст поста')
    picture = models.ImageField(verbose_name='Иллюстрация', upload_to='parsed/',
                                blank=True, null=True)
    tag = models.ManyToManyField(Tag, verbose_name='Тэги')
    posted = models.BooleanField(verbose_name='Репост осуществлён', default=False)

    ixbt_options = IxbtOptions('Настройки скреппинга ixbt.com')
    bigmir_options = BigmirOptions('Настройки скреппинга bigmir.net')
    ria_options = RiaOptions('Настройки скреппинга ria.ru')
    rbk_options = RbkOptions('Настройки скреппинга РБК')
    itworld_options = ItworldOptions('Настройки скреппинга IT World')
    comnews_options = ComnewsOptions('Настройки скреппинга Comnews')
    rosbalt_options = RosbaltOptions('Настройки скреппинга Росбалт')
    interfax_options = InterfaxOptions('Настройки скреппинга Интерфакс')
    belta_options = BeltaOptions('Настройки скреппинга БелТА')
    forbes_options = ForbesOptions('Настройки скреппинга Forbes')

    class Meta:
        ordering = ['-date', ]
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __unicode__(self):
        return self.title


autocomplete_light.register(Tag,
                            search_fields = ['^name', ],
                            attrs = {
                                'placeholder': 'Введите тэг…',
                                'data-autocomplete-minimum-characters': 1,
                            },
                            widget_attrs = {
                                'data-widget-maximum-values': 12,
                                'class': 'modern-style',
                            })
