# coding: utf-8

from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from suit_ckeditor.widgets import CKEditorWidget

from models import News, Tag


class TagAdmin(admin.ModelAdmin):
    model = Tag
    list_display = ('name', )


class NewsAdminForm(forms.ModelForm):
    class Meta:
        model = News
        widgets = { 'text': CKEditorWidget }
        exclude = {}


def mark_posted(modeladmin, request, queryset):
    queryset.update(posted=True)


def mark_unposted(modeladmin, request, queryset):
    queryset.update(posted=False)


mark_posted.short_description = 'Пометить как опубликованные'
mark_unposted.short_description = 'Пометить как неопубликованные'


class NewsAdmin(admin.ModelAdmin):
    form = NewsAdminForm
    list_display = ('id', 'date', 'title', )
    list_filter = ('date', )
    filter_horizontal = ('tag', )
    actions = [mark_posted, mark_unposted, ]


admin.site.register(News, NewsAdmin)
admin.site.register(Tag, TagAdmin)
