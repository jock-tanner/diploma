# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import dbsettings

from timezone_field import TimeZoneFormField


class TimezoneValue(dbsettings.StringValue):
    class field(TimeZoneFormField):
        pass


class IxbtOptions(dbsettings.Group):
    ixbt_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, дней')
    ixbt_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    ixbt_timezone = TimezoneValue('Часовой пояс')


class BigmirOptions(dbsettings.Group):
    bigmir_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, постов')
    bigmir_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    bigmir_timezone = TimezoneValue('Часовой пояс')


class RiaOptions(dbsettings.Group):
    ria_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    ria_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    ria_timezone = TimezoneValue('Часовой пояс')


class RbkOptions(dbsettings.Group):
    rbk_start_addresses = dbsettings.MultiSeparatorValue('Стартовые адреса (;)')


class ItworldOptions(dbsettings.Group):
    itworld_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    itworld_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')


class ComnewsOptions(dbsettings.Group):
    comnews_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    comnews_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')


class RosbaltOptions(dbsettings.Group):
    rosbalt_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    rosbalt_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    rosbalt_timezone = TimezoneValue('Часовой пояс')


class InterfaxOptions(dbsettings.Group):
    interfax_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    interfax_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    interfax_timezone = TimezoneValue('Часовой пояс')


class BeltaOptions(dbsettings.Group):
    belta_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, страниц')
    belta_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    belta_timezone = TimezoneValue('Часовой пояс')


class ForbesOptions(dbsettings.Group):
    forbes_crawl_depth = dbsettings.PositiveIntegerValue('Глубина обработки, дней')
    forbes_start_addresses = dbsettings.MultiSeparatorValue('Шаблоны стартовых адресов (;)')
    forbes_timezone = TimezoneValue('Часовой пояс')
