# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('link', models.CharField(max_length=255, verbose_name='\u041e\u0440\u0438\u0433\u0438\u043d\u0430\u043b')),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043f\u043e\u0441\u0442\u0430')),
                ('picture', models.ImageField(upload_to='parsed/', null=True, verbose_name='\u0418\u043b\u043b\u044e\u0441\u0442\u0440\u0430\u0446\u0438\u044f', blank=True)),
                ('posted', models.BooleanField(default=False, verbose_name='\u0420\u0435\u043f\u043e\u0441\u0442 \u043e\u0441\u0443\u0449\u0435\u0441\u0442\u0432\u043b\u0451\u043d')),
            ],
            options={
                'ordering': ['date'],
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=128, verbose_name='\u0422\u044d\u0433')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u0422\u044d\u0433',
                'verbose_name_plural': '\u0422\u044d\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='news',
            name='tag',
            field=models.ManyToManyField(to='scrapped.Tag', verbose_name='\u0422\u044d\u0433\u0438'),
            preserve_default=True,
        ),
    ]
