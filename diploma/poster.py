#!/usr/bin/env python
# -*- coding: utf-8 -*-

# python3 print() function and unicode strings
# функция print() и строки в Юникоде в стиле python3
from __future__ import unicode_literals, print_function

import os, sys


# инициализируем Django
# initialize Django
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '../env/lib/python2.7/site-packages/'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'diploma.settings'

# ...этот вызов необходим, начиная с версии 1.7
# ...this is required since django 1.7
import django
django.setup()

from django.conf import settings
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialApp, SocialAccount, SocialToken

from scrapped.models import News
from profile.models import UserProfile

import time, requests, urlparse, vk_api, tweepy, pytumblr
from lxml import html
from lxml.html.clean import clean_html


# this hack enables Russian characters in console/log output
# грязный способ выводить русские символы в консоль и лог
if sys.getdefaultencoding() != 'utf-8':
    reload(sys)
    sys.setdefaultencoding('utf-8')

# get site name from Sites framework
# получаем имя сайта из фреймворка Sites
SITE_URL = None

# C-style program entry point
# точка входа программы в стиле C
def main(argv):
    global SITE_URL
    SITE_URL = Site.objects.get(id=settings.SITE_ID).domain
    # получаем список неопубликованных статей
    # get list of items not yet posted
    for news in News.objects.filter(posted=False):
        # получаем тэги данной статьи
        # get item tags
        for tag in news.tag.all():
            # получаем профили пользователей, заинтересованных в данном тэге
            # get profiles of users interested in this tag
            for profile in UserProfile.objects.filter(tag=tag):
                # зарегистрирован через соцсеть?
                # registered with social account?
                for social_acc in SocialAccount.objects.filter(user=profile.user):
                    if news not in profile.posted:
                        # распределяем посты
                        # dispatch postings
                        if social_acc.provider == 'vk':
                            # не работает:
                            #do_post_in_vk(social_acc, news)
                            pass
                        elif social_acc.provider == 'twitter':
                            do_post_in_twitter(social_acc, news)
                        elif social_acc.provider == 'tumblr':
                            do_post_in_tumblr(social_acc, news)
                        # не постить один пост дважды
                        # do not post it twice
                        profile.posted.append(news)
        # mark news as posted
        # помечаем новости как опубликованные
        news.posted = True
        news.save()
    return 0


def do_post_in_vk(account, news):
    """
    Постим новость в виде текста и вложенного изображения на стену в ВК
    (код годный, но в нашем случае не работает, так как нельзя просто так
    взять и получить право доступа к стене из десктопного приложения ВК)

    :param account: учётная запись в ВК
    :param news: нововсть
    """
    print('Posting {0} in VK'.format(news))
    app = SocialApp.objects.get(provider='vk')
    app_id = app.client_id
    token = SocialToken.objects.get(account=account, app=app).token
    try:
        vk = vk_api.VkApi(app_id=app_id, token=token)
    except vk_api.AuthorizationError as e:
        print('Ошибка авторизации: {0}'.format(e))
        return
    owner_id = int(account.uid)
    values = {
        # use positive ids for users, negative for clubs & publics
        'owner_id': owner_id,
        'friends_only': 0,
        }
    if news.picture:
        try:
            # получаем параметры сервера загрузки изображений
            # get picture POST parameters
            reply1 = vk.method('photos.getWallUploadServer')
            # постим картинку
            # post picture
            reply2 = requests.post(reply1['upload_url'],
                                   files={
                                       'file': ('item.jpg', news.picture.file.read(), 'image/jpeg'),
                                       })
            # сохраняем картинку на стене
            # save photo to wall
            reply3 = vk.method('photos.saveWallPhoto', reply2.json())
        except vk_api.ApiError as e:
            print('Ошибка API: {0}'.format(e))
            return
        # делаем картинку вложением в пост
        # preparing an attachment
        values['attachments'] = ('photo{0}_{1}'.format(reply3[0]['owner_id'],
                                                       reply3[0]['id']), )
    else:
        # избавляемся от последнего вложения
        # get rid of previous attachment
        values.pop('attachments', None)

    # загружаем текст, чистим его от тэгов, удваиваем переносы строк
    # text load, cleanup, format
    dom = html.document_fromstring(news.text)
    values['message'] = dom.text_content().replace('\n', '\n\n')
    # постим
    # do post
    try:
        print(vk.method('wall.post', values))
    except vk_api.ApiError as e:
        print('Ошибка API: {0}'.format(e))
        return
    # официальный throttling limit -- не более 3 постов в секунду, но на самом деле
    # условия запроса капчи гораздо жёстче
    time.sleep(5)
    return


def do_post_in_twitter(account, news):
    """
    Постим новость в виде заголовка (или части заголовка, помещающейся в твит)
    и ссылки на оригинальную новость.

    :param account: аккаунт в соцсети
    :param news: новость
    """
    # готовим начальные параметры
    # preparing initial parameters
    social_app = SocialApp.objects.get(provider='twitter')
    consumer_key = social_app.client_id
    consumer_secret = social_app.secret
    social_token = SocialToken.objects.get(app=social_app, account=account)
    access_key = social_token.token
    access_secret = social_token.token_secret
    # authorize
    # авторизуемся
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
    status = news.title
    # обновляем статус
    # updating status
    if len(status) > 116:
        status = status[:115] + '…'
    api.update_status(status='{0}: {1}'.format(status, news.link)).text
    return


def do_post_in_tumblr(account, news):
    """
    Создаём фотопост либо текстовой пост в основном аккаунте Tumblr.

    :param account: аккаунт в соцсети
    :param news: новость
    """
    if len(news.text) == 0:
        return
    # готовим начальные параметры
    # preparing initial parameters
    social_app = SocialApp.objects.get(provider='tumblr')
    consumer_key = social_app.client_id
    consumer_secret = social_app.secret
    social_token = SocialToken.objects.get(app=social_app, account=account)
    access_key = social_token.token
    access_secret = social_token.token_secret
    # создаём клиента
    # create client
    client = pytumblr.TumblrRestClient(
        consumer_key,
        consumer_secret,
        access_key,
        access_secret,
    )
    # get user name
    # получаем имя пользователя
    user_name = client.info()['user']['name']
    if news.picture:
        # создаём фотопост
        # making photo post
        client.create_photo(user_name,
                            source='http://' + SITE_URL + news.picture.url,
                            caption=clean_html(news.text),
                            state='published')
    else:
        # создаём текстовой пост
        # making text post
        client.create_text(user_name,
                           title=news.title,
                           body=clean_html(news.text),
                           state='published')
    return


# main() sentinel
if __name__ == "__main__":
    sys.exit(main(sys.argv))
