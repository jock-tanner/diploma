# coding: utf-8

from __future__ import unicode_literals, print_function

import os, scrapy, requests, tempfile
from pytz import timezone
from dateutil import parser
from django.core.files import File

from crawler.items import NewsItem, belta_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Стартовые URL:
# http://www.belta.by/ru/all_news?page={0}


month_dict = {
    'Январь':   'jan',
    'Февраль':  'feb',
    'Март':     'mar',
    'Апрель':   'apr',
    'Май':      'may',
    'Июнь':     'jun',
    'Июль':     'jul',
    'Август':   'aug',
    'Сентябрь': 'sep',
    'Октябрь':  'oct',
    'Ноябрь':   'nov',
    'Декабрь':  'dec',
}


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    for i in range(1, belta_options.belta_crawl_depth+1):
        for start_address in belta_options.belta_start_addresses:
            yield start_address.format(i)


class BeltaSpider(scrapy.Spider):
    name = 'belta'
    allowed_domains = ['belta.by', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if 'page=' in response.url:
            for path in response.xpath('//h2[@class="news_rubric_header_inr"]/a/@href').extract():
                link = 'http://www.belta.by{0}'.format(path)
                if News.objects.filter(link=link).exists():
                    # в БД уже есть эта новость
                    # already have this news
                    pass
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            item = NewsItem()
            item['link'] = response.url
            date_str = response.xpath('//div[@class="news_date"]/text()').extract()[0][:-5]
            # делокализируем дату
            # delocalize date
            for month in month_dict.keys():
                date_str = date_str.replace(month, month_dict[month])
            unaware = parser.parse(date_str)
            aware = timezone(belta_options.belta_timezone).localize(unaware)
            item['date'] = aware.astimezone(timezone(TIME_ZONE))
            item['title'] = response.xpath('//h1[@class="main_header"]/text()').extract()[0]
            item['text'] = response.xpath('//span[@id="res_text"]').extract()[0]
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # собираем адреса картинок
            # get images' paths
            pics = response.xpath('//div[@class="t_n_im"]//img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get('http://www.belta.by{0}'.format(pics[0]))
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//div[@class="inner_news_tags"]/a/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
