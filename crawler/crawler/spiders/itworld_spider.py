# coding: utf-8

from __future__ import unicode_literals, print_function

import os, sys, datetime, scrapy, requests, tempfile
from pytz import timezone
from django.core.files import File

from crawler.items import NewsItem, itworld_options
from diploma.settings import TIME_ZONE
from scrapped.models import Tag, News


# Шаблоны стартовых URL:
# http://it-world.ru/all/?PAGEN_1={0}


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    i = 1
    while True:
        for start_address in itworld_options.itworld_start_addresses:
            yield start_address.format(i)
        i += 1
        if i > itworld_options.itworld_crawl_depth:
            break


class ITWorldSpider(scrapy.Spider):
    name = 'itworld'
    allowed_domains = ['it-world.ru', ]
    start_urls = start_urls_generator()
    handle_httpstatus_list = [404, ]

    def parse(self, response):
        if 'PAGEN_1' in response.url:
            # получаем ссылки на новости
            # extracting links from news index
            for path in response.xpath('//div[@class="content"]/div[@class="title-middle"]/a/@href').extract():
                link = 'http://it-world.ru{0}'.format(path)
                if News.objects.filter(link=link).exists():
                    # в БД уже есть эта статья
                    # already have this item
                    return
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            # обрабатываем новость
            # processing the item
            item = NewsItem()
            item['link'] = response.url
            # получаем дату. Время неизвестно, поэтому будем считать, что это UTC
            # get datetime. No time given so assume it's UTC
            date_str = response.xpath('//div[@class="detail"]//span[@class="note"]/text()').extract()[0][2:]
            item['date'] = timezone(TIME_ZONE).localize(datetime.datetime.strptime(date_str, '%d.%m.%Y'))
            # получаем заголовок
            # get title
            item['title'] = response.xpath('//div[@class="detail"]//h1/text()').extract()[0]
            # получаем текст
            # get text
            item['text'] = ' '.join([s for s in response.xpath('//div[@class="detail"]/p/span[text()="Теги:"]/../preceding-sibling::*[self::p]').extract()])
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # собираем адреса картинок
            # get images' paths
            pics = response.xpath('//div[@class="detail"]/p[@class="a_center"]/a/@href').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get('http://it-world.ru{0}'.format(pics[0]))
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//div[@class="detail"]/p/span[text()="Теги:"]/following-sibling::*/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
