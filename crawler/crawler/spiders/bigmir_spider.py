# coding: utf-8

from __future__ import unicode_literals, print_function

import os, datetime, scrapy, requests, tempfile
from pytz import timezone
from django.core.files import File

from crawler.items import NewsItem, bigmir_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Шаблон стартового URL:
# http://news.bigmir.net/?ajax=1&_tpl=last_news_feed&from=0&limit={0}

# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    for start_addr in bigmir_options.bigmir_start_addresses:
        yield start_addr.format(bigmir_options.bigmir_crawl_depth)


def ru_to_datetime(rustring):
    # datetime.strptime() не может русские месяцы
    # datetime.strptime() no can Russian months' names
    month_dict = {
        'января':    1,
        'февраля':   2,
        'марта':     3,
        'апреля':    4,
        'мая':       5,
        'июня':      6,
        'июля':      7,
        'августа,':  8,
        'сентября':  9,
        'октября':  10,
        'ноября':   11,
        'декабря':  12,
    }
    now = datetime.datetime.now(tz=timezone(bigmir_options.bigmir_timezone))
    today = now.replace(hour=0, minute=0, second=0, microsecond=0)
    rustring = rustring.replace('\xa0', ' ')
    ts = rustring.split(' ')
    if len(ts) == 2:
        ms = ts[1].split(':')
        delta = datetime.timedelta(hours=int(ms[0]), minutes=int(ms[1]))
        if ts[0] == 'Сегодня,':
            result = today + delta
        elif ts[0] == 'Вчера,':
            result = today + datetime.timedelta(days=-1) + delta
    elif ts[2] == 'назад':
        # [0-60] минут назад
        result = now - datetime.timedelta(minutes=int(ts[0]))
    else:
        # 20 января, 17:08
        ms = ts[2].split(':')
        result = datetime.datetime(year=today.year,
                                   month=month_dict[ts[1].replace(',', '')],
                                   day=int(ts[0]),
                                   hour=int(ms[0]),
                                   minute=int(ms[1]))
    return result.astimezone(timezone(TIME_ZONE))


class BigmirSpider(scrapy.Spider):
    name = 'bigmir'
    allowed_domains = ['bigmir.net', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if '?ajax=' in response.url:
            for path in response.xpath('//a[@class="b-last-news-feed__list-item__title"]/@href').extract():
                link = 'http://news.bigmir.net{0}'.format(path)
                if News.objects.filter(link=link):
                    # в БД уже есть эта статья
                    # already have this item
                    return
                else:
                    # save link to cope with redirect to subdomain
                    # сохраняем ссылку, так как новость может редиректить на поддомен
                    request = scrapy.Request(link, callback=self.parse)
                    request.meta['link'] = link
                    yield request
        elif 'afisha.bigmir.net' in response.url:
            # это не новости, да и формат другой
            return
        else:
            item = NewsItem()
            item['link'] = response.meta['link']
            item['date'] = ru_to_datetime(response.xpath('//time[@class="b-article-metadata__element b-article-metadata__element_datetime"]/text()').extract()[0])
            item['title'] = response.xpath('//article[@class="b-article__body"]/h1[@class="b-article__title"]/text()').extract()[0].encode('utf-8')
            item['text'] = ' '.join([s for s in response.xpath('//div[@class="b-article__description"] | '
                                                               '//div[@class="b-article__text _articleContent"]/p[not(@class)] | '
                                                               '//div[@class="b-article__text _articleContent"]/div[not(@class)]').extract()]).encode('utf-8')
            real_item = item.save()
            pics = response.xpath('//img[@class="b-article-figure__image"]/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            tags = response.xpath('//a[@class="b-article__tags-item"]/text()').extract()
            for tag_name in tags:
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
