# coding: utf-8

from __future__ import unicode_literals

import os, datetime, scrapy, requests, tempfile
from pytz import timezone
from django.core.files import File

from crawler.items import NewsItem, ria_options
from diploma.settings import TIME_ZONE
from scrapped.models import Tag, News


# Шаблоны стартовых URL:
# http://m.ria.ru/index_{0}.html (1..n)


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    for i in range(1, ria_options.ria_crawl_depth + 1):
        for start_address in ria_options.ria_start_addresses:
            yield start_address.format(i)


class RIASpider(scrapy.Spider):
    name = 'ria'
    allowed_domains = ['ria.ru', ]
    start_urls = start_urls_generator()
    download_delay = 2

    def parse(self, response):
        if 'index_' in response.url:
            # получаем ссылки на новости
            # extracting links from news index
            for news_item in response.xpath('//ul[@class="lenta-news"]/li'):
                path = news_item.xpath('.//a/@href').extract()[0]
                link = "http://ria.ru{0}".format(path)
                if News.objects.filter(link=link):
                    # в БД уже есть эта статья
                    # already have this item
                    return
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            # обрабатываем новость
            # processing the item
            item = NewsItem()
            item['link'] = response.url
            # получаем дату/время
            # get datetime
            datetime_str = response.xpath('//time[@class="article_header_date"]/@datetime').extract()[0]
            unaware = datetime.datetime.strptime(datetime_str, '%Y-%m-%dT%H:%M')
            aware = timezone(ria_options.ria_timezone).localize(unaware)
            item['date'] = aware.astimezone(timezone(TIME_ZONE))
            # получаем заголовок
            # get title
            item['title'] = response.xpath('//h1[@class="article_header_title"]/text()').extract()[0].encode('utf-8')
            # получаем текст
            # get text
            item['text'] = ' '.join([s for s in response.xpath('//*[@id="article_full_text"]/p | //*[@id="article_full_text"]/h3').extract()]).encode('utf-8')
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # получаем картинку
            # get image
            pics = response.xpath('//div[@class="article_illustration"]//img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//ul[@class="article_tags_overview_list"]/li[@class="article_tags_overview_list_item"]/a/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
