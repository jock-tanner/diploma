# coding: utf-8

from __future__ import unicode_literals, print_function

import os, scrapy, requests, tempfile
from pytz import timezone
from dateutil import parser
from django.core.files import File
from scrapy.contrib.spiders import XMLFeedSpider

from crawler.items import NewsItem, rbk_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Стартовые URL:
# http://static.feed.rbc.ru/rbc/internal/rss.rbc.ru/rbc.ru/news.rss


class RBKSpider(XMLFeedSpider):
    """
    Этот краулер берёт посты из RSS-ленты.
    """
    name = 'rbk'
    allowed_domains = ['rbc.ru', ]
    start_urls = rbk_options.rbk_start_addresses

    def parse_node(self, response, selector):
        # получаем ссылку на новость без фрагмента
        # get news link without fragment
        link = selector.xpath('link/text()').extract()[0].split('#')[0]
        if len(News.objects.filter(link=link)) == 0:
            # в БД ещё нет этой статьи
            # no such item in the database
            request = scrapy.Request(link, callback=self.parse_news)
            aware = parser.parse(selector.xpath('pubDate/text()').extract()[0])
            request.meta['date'] = aware.astimezone(timezone(TIME_ZONE))
            return request

    def parse_news(self, response):
        item = NewsItem()
        item['link'] = response.url
        item['date'] = response.meta['date']
        item['title'] = response.xpath('//div[@class="article__overview__title"]/span/text()').extract()[0]
        item['text'] = ' '.join([s for s in response.xpath('//div[@class="article__text"]//p').extract()])
        real_item = item.save()
        pics = response.xpath('//div[@class="article__main-image"]/img/@src').extract()
        if len(pics) > 0:
            # есть картинки, берём первую
            # images found, get the first on
            pic = requests.get(pics[0])
            # сохраняем картинку во временный файл
            # save image to temporary file
            tmp = tempfile.NamedTemporaryFile()
            for chunk in pic.iter_content(4096):
                tmp.write(chunk)
            # из временного файла отправляем картинку на её законное место
            # copy picture from temporary file to its final destination
            real_item.picture.save(os.path.basename(pic.url), File(tmp))
            tmp.close()
        tags = response.xpath('//a[@class="article__tags__link"]/text()').extract()
        for tag_name in tags:
            # ищем или создаём тэг
            # get or create tag
            tag_name = tag_name.upper().rstrip(', ')
            tag, created = Tag.objects.get_or_create(name=tag_name)
            if created:
                tag.save()
            # отмечаем новость тэгом
            # tag news
            real_item.tag.add(tag)
        yield item
