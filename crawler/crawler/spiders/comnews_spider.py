# coding: utf-8

from __future__ import unicode_literals, print_function

import os, scrapy, requests, tempfile
from pytz import timezone
from dateutil import parser
from django.core.files import File

from crawler.items import NewsItem, comnews_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Шаблон стартового URL:
# http://www.comnews.ru/news?page={0}

# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    i = 0
    while True:
        for start_url in comnews_options.comnews_start_addresses:
            yield start_url.format(i)
        i += 1
        if i >= comnews_options.comnews_crawl_depth:
            break


class ComnewsSpider(scrapy.Spider):
    name = 'comnews'
    allowed_domains = ['comnews.ru', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if 'page=' in response.url:
            for path in response.xpath('//span[@class="field-content title"]/a/@href').extract():
                # получаем полный адрес новости
                # get news' full URL
                link = 'http://www.comnews.ru{0}'.format(path)
                if News.objects.filter(link=link).exists():
                    # в БД уже есть эта новость
                    # already have this news
                    pass
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            item = NewsItem()
            item['link'] = response.url
            aware = parser.parse(response.xpath('//span[@class="date-display-single"]/@content').extract()[0])
            item['date'] = aware.astimezone(timezone(TIME_ZONE))
            item['title'] = response.xpath('//h1[@property="dc:title"]/text()').extract()[0]
            item['text'] = ' '.join([s for s in response.xpath('//div[@property="content:encoded"]/p').extract()])
            real_item = item.save()
            pics = response.xpath('//div[@class="field-item even big-pix"]/img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            tags = response.xpath('//div[@class="headerTags"]/span[@class="term"]/a/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
