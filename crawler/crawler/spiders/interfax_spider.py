# coding: utf-8

from __future__ import unicode_literals, print_function

import os, scrapy, requests, tempfile
from pytz import timezone
from dateutil import parser
from django.core.files import File

from crawler.items import NewsItem, interfax_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Стартовые URL:
# http://www.interfax.ru/txtall.asp?p={0}


month_dict = {
    'января':   'jan',
    'февраля':  'feb',
    'марта':    'mar',
    'апреля':   'apr',
    'мая':      'may',
    'июня':     'jun',
    'июля':     'jul',
    'августа':  'aug',
    'сентября': 'sep',
    'октября':  'oct',
    'ноября':   'nov',
    'декабря':  'dec',
}


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    for i in range(1, interfax_options.interfax_crawl_depth + 1):
        for start_address in interfax_options.interfax_start_addresses:
            yield start_address.format(i)


class InterfaxSpider(scrapy.Spider):
    name = 'interfax'
    allowed_domains = ['interfax.ru', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if 'txtall.asp' in response.url:
            for path in response.xpath('//a[@class="bigbluehead"]/@href').extract():
                link = 'http://www.interfax.ru{0}'.format(path)
                if News.objects.filter(link=link).exists():
                    # в БД уже есть эта новость
                    # already have this news
                    pass
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            item = NewsItem()
            item['link'] = response.url
            date_selectors = response.xpath('//div[@class="img700_date"]/div')
            for date_selector in date_selectors:
                date_str = ''.join([s for s in date_selector.xpath('descendant::*/text()').extract()])
                if 'года' in date_str:
                    # подходит
                    # valid
                    for month in month_dict.keys():
                        date_str = date_str.replace(month, month_dict[month])
                    date_str = date_str.replace(' года', '').replace('\xa0', ' ')
                    unaware = parser.parse(date_str)
                    aware = timezone(interfax_options.interfax_timezone).localize(unaware)
                    item['date'] = aware.astimezone(timezone(TIME_ZONE))
                    break
            item['title'] = response.xpath('//h1[@itemprop="name"]/text()').extract()[0]
            item['text'] = ' '.join([s for s in response.xpath('//div[@class="img700_announce"] | '
                                                               '//div[contains(@class,"txtmain")]/p').extract()])
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # собираем адреса картинок
            # get images' paths
            pics = response.xpath('//div[@class="img700"]/img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//td[@class="f12"]/a[@class="f12 lnkblue"]/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item

