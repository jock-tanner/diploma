# coding: utf-8

from __future__ import unicode_literals, print_function

import os, scrapy, requests, datetime, tempfile
from pytz import timezone
from django.core.files import File

from crawler.items import NewsItem, forbes_options
from scrapped.models import Tag, News
from diploma.settings import TIME_ZONE


# Стартовые URL:
# http://www.forbes.ru/forbes-today?date[value][year]={0}&date[value][month]={1}&date[value][day]={2}


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    now = datetime.datetime.now(timezone(forbes_options.forbes_timezone))
    for i in range(forbes_options.forbes_crawl_depth):
        for start_address in forbes_options.forbes_start_addresses:
            yield start_address.format(now.year, now.month, now.day)
        now = now - datetime.timedelta(days=1)


class ForbesSpider(scrapy.Spider):
    name = 'forbes'
    allowed_domains = ['forbes.ru', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if 'date%5Bvalue%5D' in response.url:
            for path in response.xpath('//div[contains(@class,"publicline")]/h3/a/@href').extract():
                link = 'http://www.forbes.ru{0}'.format(path)
                if News.objects.filter(link=link).exists():
                    # в БД уже есть эта новость
                    # already have this news
                    pass
                else:
                    yield scrapy.Request(link, callback=self.parse)
        elif 'forbeslife-photogallery' in response.url:
            return
        else:
            item = NewsItem()
            item['link'] = response.url
            date_str = response.xpath('//div[@class="date"]/b/text()').extract()[0] + \
                       response.xpath('//div[@class="date"]/text()').extract()[0]
            unaware = datetime.datetime.strptime(date_str, '%d.%m.%Y %H:%M')
            aware = timezone(forbes_options.forbes_timezone).localize(unaware)
            item['date'] = aware.astimezone(timezone(TIME_ZONE))
            item['title'] = response.xpath('//h1/text()').extract()[0]
            item['text'] = ' '.join([s for s in response.xpath('//div[contains(@class,"field-field-subtitle")]/div/div |'
                                                               ' //div[@class="article"]/p').extract()])
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # собираем адреса картинок
            # get images' paths
            pics = response.xpath('//div[contains(@class,"field-field-main-image")]/div/div/img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//div[@class="tags"]/a/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
