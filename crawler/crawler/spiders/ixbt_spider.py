# coding: utf-8

import os, datetime, scrapy, requests, tempfile
from pytz import timezone
from django.core.files import File

from crawler.items import NewsItem, ixbt_options
from diploma.settings import TIME_ZONE
from scrapped.models import Tag, News


# Шаблоны стартовых URL:
# http://www.ixbt.com/news/hard/archive.shtml?{year}/{month}{day};
# http://www.ixbt.com/news/soft/archive.shtml?{year}/{month}{day}


# генерируем список стартовых адресов
# generate list of start URLs
def start_urls_generator():
    parse_date = datetime.date.today()
    i = ixbt_options.ixbt_crawl_depth
    while True:
        for start_addr in ixbt_options.ixbt_start_addresses:
            # конструируем запрос
            # make up request URL
            yield start_addr.format(year=parse_date.strftime('%Y'),
                                    month=parse_date.strftime('%m'),
                                    day=parse_date.strftime('%d'))
        # повторяем для предыдущего дня, пока не достигнем нужной глубины
        # repeat for the day back until the given depth is reached
        i -= 1
        if i > 0:
            parse_date -= datetime.timedelta(days=1)
        else:
            break


class IXBTSpider(scrapy.Spider):
    name = 'ixbt'
    allowed_domains = ['ixbt.com', ]
    start_urls = start_urls_generator()

    def parse(self, response):
        if 'archive.shtml' in response.url:
            # получаем ссылки на новости
            # extracting links from news index
            for path in response.xpath('//a[@class="nl_nw_link"]/@href').extract():
                link = "http://www.ixbt.com{0}".format(path)
                if News.objects.filter(link=link):
                    # в БД уже есть эта статья
                    # already have this item
                    return
                else:
                    yield scrapy.Request(link, callback=self.parse)
        else:
            # обрабатываем новость
            # processing the item
            item = NewsItem()
            item['link'] = response.url
            # получаем дату/время
            # get datetime
            if 'news/hard/' in response.url:
                # 'hard' timestamp format
                time_str = response.xpath('//span[@itemprop="datePublished"]/text()').extract()[0].replace('"', '').strip()
                date_str = response.xpath('//span[@itemprop="datePublished"]/span/text()').extract()[0]
            else:
                # 'soft' timestamp format
                time_str = response.xpath('//span[@class="datetime"]/text()').extract()[0].replace('"', '').strip()
                date_str = response.xpath('//span[@class="datetime"]/span/text()').extract()[0]
            unaware = datetime.datetime.strptime(' '.join([date_str, time_str, ]), '%d.%m.%Y %H:%M')
            aware = timezone(ixbt_options.ixbt_timezone).localize(unaware)
            item['date'] = aware.astimezone(timezone(TIME_ZONE))
            # получаем заголовок
            # get title
            item['title'] = response.xpath('//div[@class="news_body"]/h1/text()').extract()[0].encode('utf-8')
            # текст состоит из параграфов и дивов
            # get all paragraphs and <div> blocks
            item['text'] = ' '.join([s for s in response.xpath('//div[@class="news_body"]//p | //div[@class="news_body"]//div').extract()]).encode('utf-8')
            # метод DjangoItem для сохранения данных в БД возвращает запись
            # DjangoItem extra method: save item to database
            real_item = item.save()
            # собираем адреса картинок
            # get images' paths
            pics = response.xpath('//div[@class="news_body"]//img/@src').extract()
            if len(pics) > 0:
                # есть картинки, берём первую
                # images found, get the first on
                pic = requests.get(pics[0])
                # сохраняем картинку во временный файл
                # save image to temporary file
                tmp = tempfile.NamedTemporaryFile()
                for chunk in pic.iter_content(4096):
                    tmp.write(chunk)
                # из временного файла отправляем картинку на её законное место
                # copy picture from temporary file to its final destination
                real_item.picture.save(os.path.basename(pic.url), File(tmp))
                tmp.close()
            # собираем тэги
            # get tags
            tags = response.xpath('//ul[@class="tagline"]/li/a/text()').extract()
            for tag_name in tags:
                # ищем или создаём тэг
                # get or create tag
                tag_name = tag_name.upper()
                tag, created = Tag.objects.get_or_create(name=tag_name)
                if created:
                    tag.save()
                # отмечаем новость тэгом
                # tag news
                real_item.tag.add(tag)
            yield item
