# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import os, sys
from scrapy.contrib.djangoitem import DjangoItem

# инициализируем Django
# initialize Django
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../diploma/'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'diploma.settings'

# ...этот вызов необходим, начиная с версии 1.7
# ...this is required since django 1.7
import django
django.setup()

# импортируем модель Django
# import model from Django app
from scrapped.models import News

# определим формат новости для парсинга
# item definition
class NewsItem(DjangoItem):
    django_model = News

# заодно получим опции модели
# get model options (django-dbsettings)
ixbt_options = News.ixbt_options
bigmir_options = News.bigmir_options
ria_options = News.ria_options
rbk_options = News.rbk_options
itworld_options = News.itworld_options
comnews_options = News.comnews_options
rosbalt_options = News.rosbalt_options
interfax_options = News.interfax_options
belta_options = News.belta_options
forbes_options = News.forbes_options
