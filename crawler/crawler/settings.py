# -*- coding: utf-8 -*-

# Scrapy settings for ixbt project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

# уберём отладочные сообщения
LOG_LEVEL = 'WARNING'

BOT_NAME = 'crawler'

SPIDER_MODULES = ['crawler.spiders']
NEWSPIDER_MODULE = 'crawler.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = r'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36'

# похоже, что сайт РИА не любит многопоточность
CONCURRENT_REQUESTS_PER_DOMAIN = 2
